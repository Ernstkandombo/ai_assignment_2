### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# ╔═╡ d2843818-0ae2-4999-a537-3febe0911e89
using Pkg

# ╔═╡ cabc55e7-d2cf-4de4-801c-27f1ca403e3c
using PlutoUI

# ╔═╡ 90bbf736-d79a-4786-895c-41d8412ca3d9
using DataStructures

# ╔═╡ 73e9c59b-0b87-426c-94ce-4d2ce12cbf68
using Markdown

# ╔═╡ c05588a4-b0ca-4609-a846-acc705e3896b
using InteractiveUtils

# ╔═╡ 4fb52f61-d722-4262-982d-80ab9ab6af43
using Printf

# ╔═╡ 2a75fd6d-a38c-4461-8aaa-560e3ea40f21
@enum Domain a b c d e

# ╔═╡ 5ab111d1-a77d-48c5-a8dc-f3f6f58a0c91
abstract type AbstractCSP
end

# ╔═╡ 93948088-a078-43f9-8b79-d5383a368a08
struct Constant_Function_Dict{A}
    value::A
end

# ╔═╡ b78daab0-08f8-4ae4-9a16-eac0a750f7da
mutable struct CSPDictionary
    dict::Union{Nothing, Dict, Constant_Function_Dict}
end

# ╔═╡ 68cc870c-9def-4dcc-bbe1-38deb0057e13
mutable struct CSP <: AbstractCSP
	vars::AbstractVector
	domains::CSPDictionary
	neighbors::CSPDictionary
	constraints::Function
	initial::Tuple
	current_domains::Union{Nothing, Dict}
	nassigns::Int64
end

# ╔═╡ 97025967-16de-4f9c-993f-4ae7a5e7b5e7
mutable struct CSPVar
	name::String
	value::Union{Nothing,Domain}
	forbidden_values::Vector{Domain}
	domain_count::Int64
end

# ╔═╡ c785e75c-2f71-4427-b0b4-e5b2a7b428f4
struct TheCSP
	vars::Vector{CSPVar}
	constraints::Vector{Tuple{CSPVar,CSPVar}}
end

# ╔═╡ 21a79d2e-0ac2-4225-a7b0-afd706091622
struct graph{T <: Real,U}
    edges::Dict{Tuple{U,U},T}
    verts::Set{U}
end

# ╔═╡ 822cd9d4-115e-490d-b4b3-02aa3a9ab678
function Cons(vars::AbstractVector, domains::CSPDictionary, neighbors::CSPDictionary, constraints::Function;
                initial::Tuple=(), current_domains::Union{Nothing, Dict}=nothing, nassigns::Int64=0)
        return new(vars, domains, neighbors, constraints, initial, current_domains, nassigns)
    end

# ╔═╡ f93fb1ae-7428-4fa6-b74a-baec7d30f48f
function consGraph(edges::Vector{Tuple{U,U,T}}) where {T <: Real,U}
    vnames = Set{U}(v for edge in edges for v in edge[1:2])
    adjmat = Dict((edge[1], edge[2]) => edge[3] for edge in edges)
    return graph(adjmat, vnames)
end

# ╔═╡ d180e088-48dd-4612-80d8-90ecb54576c4
begin
	vertices(g::graph) = g.verts
	edges(g::graph)    = g.edges
end

# ╔═╡ 78df7cd5-ff15-4420-bf59-0613b898ed01
diff = rand(setdiff(Set([a,b,c,d]), Set([a,b])))

# ╔═╡ 162cd837-61cc-4616-a202-968bdd2e5839
neighbours(g::graph, v) = Set((b, c) for ((a, b), c) in edges(g) if a == v)

# ╔═╡ 4e3e62a3-43d5-4f2b-bb65-fd6eccccff4e
function assigning(problem::T, key, val, assignment::Dict) where {T <: AbstractCSP}
    assignment[key] = val;
    problem.nassigns = problem.nassigns + 1;
    nothing;
end

# ╔═╡ b2905605-efa3-4371-b091-7182180142bd
function unassigning(problem::T, key, assignment::Dict) where {T <: AbstractCSP}
    if (haskey(assignment, key))
        delete!(assignment, key);
    end
    nothing;
end

# ╔═╡ c7904777-1d22-4ab1-80e6-6b0dcab7f514
function error(problem::T, key, val, assignment::Dict) where {T <: AbstractCSP}
    return count(
                (function(second_key)
                    return (haskey(assignment, second_key) &&
                        !(problem.constraints(key, val, second_key, assignment[second_key])));
                end),
                problem.neighbors[key]);
end

# ╔═╡ 8c41d7bf-0c9d-4d49-a2ec-e937c8bd02b3
function display(problem::T, assignment::Dict) where {T <: AbstractCSP}
    println("Cons: ", problem, " with assignment: ", assignment);
    nothing;
end

# ╔═╡ 6a8f805b-49d0-46a5-aea7-e6afd4029b2f
function actions(problem::T, state::Tuple) where {T <: AbstractCSP}
    if (length(state) == length(problem.vars))
        return [];
    else
        let
            local assignment = Dict(state);
            local var = problem.vars[findfirst((function(e)
                                        return !haskey(assignment, e);
                                    end), problem.vars)];
            return collect((var, val) for val in problem.domains[var]
                            if error(problem, var, val, assignment) == 0);
        end
    end
end

# ╔═╡ ed434872-a6a1-45c8-9b36-98445e660447
function print_result(problem::T, state::Tuple, action::Tuple) where {T <: AbstractCSP}
    return (state..., act);
end

# ╔═╡ 2f47b853-4b26-4eef-a8a4-17d750955c8b
function checkpath(g::graph{T,U}, source::U, dest::U) where {T, U}
    @assert source ∈ vertices(g) "$source is not a vertex in the graph"
 
    if source == dest return [source], 0 end
    # Initialize variables
    inf  = typemax(T)
    dist = Dict(v => inf for v in vertices(g))
    prev = Dict(v => v   for v in vertices(g))
    dist[source] = 0
    Q = copy(vertices(g))
    neigh = Dict(v => neighbours(g, v) for v in vertices(g))
 
    # Main loop
    while !isempty(Q)
        u = reduce((x, y) -> dist[x] < dist[y] ? x : y, Q)
        pop!(Q, u)
        if dist[u] == inf || u == dest break end
        for (v, cost) in neigh[u]
            alt = dist[u] + cost
            if alt < dist[v]
                dist[v] = alt
                prev[v] = u
            end
        end
    end
 
    # Return path
    rst, cost = U[], dist[dest]
    if prev[dest] == dest
        return rst, cost
    else
        while dest != source
            pushfirst!(rst, dest)
            dest = prev[dest]
        end
        pushfirst!(rst, dest)
        return rst, cost
    end
end

# ╔═╡ 5d94077c-d72a-41cc-af17-6da3daef738f
md"##### goalTest"

# ╔═╡ 228e7f79-036e-4564-aae2-e5b9b8b64ae5
function goalTest(problem::T, state::Tuple) where {T <: AbstractCSP}
    let
        local assignment = Dict(state);
        return (length(assignment) == length(problem.vars) &&
                all((function(key)
                            return error(problem, key, assignment[key], assignment) == 0;
                        end)
                        ,
                        problem.vars));
    end
end

# ╔═╡ e15b084d-ea4a-47d6-9701-783f83026d47
function goalTest(problem::T, state::Dict) where {T <: AbstractCSP}
    let
        local assignment = deepcopy(state);
        return (length(assignment) == length(problem.vars) &&
                all((function(key)
                            return error(problem, key, assignment[key], assignment) == 0;
                        end),
                        problem.vars));
    end
end

# ╔═╡ a8f9ef85-20a6-4ee8-b88b-0932cccafbe1
function path_cost(problem::T, cost::Float64, state1::Tuple, action::Tuple, state2::Tuple) where {T <: AbstractCSP}
    return cost + 1;
end

# ╔═╡ 0b311069-03fe-45fe-aaed-c46d9f35178a
function learn(problem::T) where {T <: AbstractCSP}
    if (problem.current_domains === nothing)
        problem.current_domains = Dict(collect(Pair(key, collect(problem.domains[key])) for key in problem.vars));
    end
    nothing;
end

# ╔═╡ 057589d5-8f0d-48ce-814e-cf423e9ec924
function tough(problem::T, key, val) where {T <: AbstractCSP}
    support_pruning(problem);
    local removals::AbstractVector = collect(Pair(key, a) for a in problem.current_domains[key]
                                            if (a != val));
    problem.current_domains[key] = [val];
    return removals;
end

# ╔═╡ ae2bb0f4-f102-4e66-b7e8-a8c02b211577
function pruning(problem::T, key, value, removals) where {T <: AbstractCSP}
    local not_removed::Bool = true;
    for (i, element) in enumerate(problem.current_domains[key])
        if (element == value)
            deleteat!(problem.current_domains[key], i);
            not_removed = false;
            break;
        end
    end
    if (not_removed)
        error("Could not find ", value, " in ", problem.current_domains[key], " for key '", key, "' to be removed!");
    end
    if (!(typeof(removals) <: Nothing))
        push!(removals, Pair(key, value));
    end
    nothing;
end

# ╔═╡ 44cdfff8-d323-41dc-bfda-a96ac0af702b
function select(problem::T, key) where {T <: AbstractCSP}
    if (!(problem.current_domains === nothing))
        return problem.current_domains[key];
    else
        return problem.domains[key];
    end
end

# ╔═╡ d9a84bef-79d2-4824-a621-75236d07be4e
function assignment(problem::T) where {T <: AbstractCSP}
    support(problem);
    return Dict(collect(Pair(key, problem.current_domains[key][1])
                        for key in problem.vars
                            if (1 == length(problem.current_domains[key]))));
end

# ╔═╡ cc86c76c-2948-4bd8-8eba-49476eb5ddea
function restore(problem::T, removals::AbstractVector) where {T <: AbstractCSP}
    for (key, val) in removals
        push!(problem.current_domains[key], val);
    end
    nothing;
end

# ╔═╡ fa682d70-837d-41be-9dc1-573a6537febf
function errorvariables(problem::T, current_assignment::Dict) where {T <: AbstractCSP}
    return collect(var for var in problem.vars
                    if (error(problem, var, current_assignment[var], current_assignment) > 0));
end

# ╔═╡ 6eb51dfa-32ff-44f4-811d-a755d06f93bf
md"##### forward checkin"

# ╔═╡ 356dc299-88c0-486f-8045-63be309c0349
function forward_checking(problem::T, var, value, assignment::Dict, removals::Union{Nothing, AbstractVector}) where {T <: AbstractCSP}
    for B in problem.neighbors[var]
        if (!haskey(assignment, B))
            for b in copy(problem.current_domains[B])
                if (!problem.constraints(var, value, B, b))
                    pruning(problem, B, b, removals);
                end
            end
            if (length(problem.current_domains[B]) == 0)
                return false;
            end
        end
    end
    return true;
end

# ╔═╡ a7a1ca29-4f04-4e47-885c-ae674d474b1c
md"##### backtrack"

# ╔═╡ 41105537-c83d-4570-8e6c-1529922c5d70
function backtrack(problem::T, assignment::Dict;
                    select_unassigned_variable::Function=first_unassigned_variable,
                    order_domain_values::Function=unordered_domain_values,
                    inference::Function=no_inference) where {T <: AbstractCSP}
    if (length(assignment) == length(problem.vars))
        return assignment;
    end
    local var = select_unassigned_variable(problem, assignment);
    for value in order_domain_values(problem, var, assignment)
        if (nconflicts(problem, var, value, assignment) == 0)
            assign(problem, var, value, assignment);
            removals = suppose(problem, var, value);
            if (inference(problem, var, value, assignment, removals))
                result = backtrack(problem, assignment,
                                    select_unassigned_variable=select_unassigned_variable,
                                    order_domain_values=order_domain_values,
                                    inference=inference);
                if (!(typeof(result) <: Nothing))
                    return result;
                end
            end
            reserve(problem, removals);
        end
    end
    unassign(problem, var, assignment);
    return nothing;
end

# ╔═╡ 9be3d7ca-83bc-4963-a586-c9c16ede805f
testconsGraph = [("a", "b", 1), ("b", "e", 2), ("a", "e", 4)]

# ╔═╡ 6ffa9359-58d6-4276-94a1-11f28ee72652
g = consGraph(testconsGraph)

# ╔═╡ c79561cc-90d0-412e-ab3e-c29f59e0b09e
src, dst = "a", "e"

# ╔═╡ 3f8400cd-0a69-48ab-a5f5-113d749efcf7
path, cost = checkpath(g, src, dst)

# ╔═╡ 6429881d-c9ec-41bc-ab05-6395a72f03a9
with_terminal() do
	println("Shortest path from $src to $dst: ", isempty(path) ? "no possible path" : join(path, " → "), " (cost $cost)")
end

# ╔═╡ 69003540-f8fa-4fdb-a346-a786aa4109f9
with_terminal() do
	@printf("\n%3s | %2s | %s\n", "src", "dst", "path")
    @printf("'''''''''''''''''\n")
	for src in vertices(g), dst in vertices(g)
    path, cost = checkpath(g, src, dst)
    @printf("%3s | %2s | %s\n", src, dst, isempty(path) ? "no possible path" : join(path, " → ") * " ($cost)")
	end
end

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
InteractiveUtils = "b77e0a4c-d291-57a0-90e8-8db25a27a240"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"
Pkg = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
Printf = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═d2843818-0ae2-4999-a537-3febe0911e89
# ╠═cabc55e7-d2cf-4de4-801c-27f1ca403e3c
# ╠═90bbf736-d79a-4786-895c-41d8412ca3d9
# ╠═73e9c59b-0b87-426c-94ce-4d2ce12cbf68
# ╠═c05588a4-b0ca-4609-a846-acc705e3896b
# ╠═4fb52f61-d722-4262-982d-80ab9ab6af43
# ╠═2a75fd6d-a38c-4461-8aaa-560e3ea40f21
# ╠═5ab111d1-a77d-48c5-a8dc-f3f6f58a0c91
# ╠═93948088-a078-43f9-8b79-d5383a368a08
# ╠═b78daab0-08f8-4ae4-9a16-eac0a750f7da
# ╠═68cc870c-9def-4dcc-bbe1-38deb0057e13
# ╠═97025967-16de-4f9c-993f-4ae7a5e7b5e7
# ╠═c785e75c-2f71-4427-b0b4-e5b2a7b428f4
# ╠═21a79d2e-0ac2-4225-a7b0-afd706091622
# ╠═822cd9d4-115e-490d-b4b3-02aa3a9ab678
# ╠═f93fb1ae-7428-4fa6-b74a-baec7d30f48f
# ╠═d180e088-48dd-4612-80d8-90ecb54576c4
# ╠═78df7cd5-ff15-4420-bf59-0613b898ed01
# ╠═162cd837-61cc-4616-a202-968bdd2e5839
# ╠═4e3e62a3-43d5-4f2b-bb65-fd6eccccff4e
# ╠═b2905605-efa3-4371-b091-7182180142bd
# ╠═c7904777-1d22-4ab1-80e6-6b0dcab7f514
# ╠═8c41d7bf-0c9d-4d49-a2ec-e937c8bd02b3
# ╠═6a8f805b-49d0-46a5-aea7-e6afd4029b2f
# ╠═ed434872-a6a1-45c8-9b36-98445e660447
# ╠═2f47b853-4b26-4eef-a8a4-17d750955c8b
# ╠═5d94077c-d72a-41cc-af17-6da3daef738f
# ╠═228e7f79-036e-4564-aae2-e5b9b8b64ae5
# ╠═e15b084d-ea4a-47d6-9701-783f83026d47
# ╠═a8f9ef85-20a6-4ee8-b88b-0932cccafbe1
# ╠═0b311069-03fe-45fe-aaed-c46d9f35178a
# ╠═057589d5-8f0d-48ce-814e-cf423e9ec924
# ╠═ae2bb0f4-f102-4e66-b7e8-a8c02b211577
# ╠═44cdfff8-d323-41dc-bfda-a96ac0af702b
# ╠═d9a84bef-79d2-4824-a621-75236d07be4e
# ╠═cc86c76c-2948-4bd8-8eba-49476eb5ddea
# ╠═fa682d70-837d-41be-9dc1-573a6537febf
# ╠═6eb51dfa-32ff-44f4-811d-a755d06f93bf
# ╠═356dc299-88c0-486f-8045-63be309c0349
# ╠═a7a1ca29-4f04-4e47-885c-ae674d474b1c
# ╠═41105537-c83d-4570-8e6c-1529922c5d70
# ╠═9be3d7ca-83bc-4963-a586-c9c16ede805f
# ╠═6ffa9359-58d6-4276-94a1-11f28ee72652
# ╠═c79561cc-90d0-412e-ab3e-c29f59e0b09e
# ╠═3f8400cd-0a69-48ab-a5f5-113d749efcf7
# ╠═6429881d-c9ec-41bc-ab05-6395a72f03a9
# ╠═69003540-f8fa-4fdb-a346-a786aa4109f9
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
