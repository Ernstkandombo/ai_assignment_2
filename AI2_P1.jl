### A Pluto.jl notebook ###
# v0.18.4

using Markdown
using InteractiveUtils

# This Pluto notebook uses @bind for interactivity. When running this notebook outside of Pluto, the following 'mock version' of @bind gives bound variables a default value (instead of an error).
macro bind(def, element)
    quote
        local iv = try Base.loaded_modules[Base.PkgId(Base.UUID("6e696c72-6542-2067-7265-42206c756150"), "AbstractPlutoDingetjes")].Bonds.initial_value catch; b -> missing; end
        local el = $(esc(element))
        global $(esc(def)) = Core.applicable(Base.get, el) ? Base.get(el) : iv(el)
        el
    end
end

# ╔═╡ 1d776b4a-1d8b-4607-b984-34b2005fd007
using Pkg

# ╔═╡ 2e727944-794f-46f7-b80a-8774333f8f2a
using PlutoUI

# ╔═╡ dc8c4de1-0f87-42d4-94c8-b2730dca78d9
using DataStructures

# ╔═╡ c40ffb8b-f1f6-48b2-892b-3104f8b14e48
using Markdown

# ╔═╡ 6f0c6169-02fa-4ef3-89f7-010b4e589829
using InteractiveUtils

# ╔═╡ b7802b24-5088-4b72-b5fd-136af885cbd0
md"### Creating State Definition"

# ╔═╡ 493da480-1b9b-451b-909a-eeaa53fc51ed
struct State
    name::String
    position::Int64
    parcel::Vector{Bool}
end

# ╔═╡ 9d97ebfa-4d7a-441f-92c1-e5724855f5b8
md"### Creating Action Definition"

# ╔═╡ 1b1461de-b58f-45a8-9043-bf6e0b10749b
struct Action
    name::String
    cost::Int64
end

# ╔═╡ 2d6488f7-743b-44af-87be-d535f5b0924a
md"### Defining Actions & Cost"

# ╔═╡ 8dc02067-c4c1-4b5f-879e-e181530e8b4d
me = Action("move_east", 2)

# ╔═╡ c35c205e-36c8-4faf-8578-7711cd9ba8b8
mw = Action("move_west", 2)

# ╔═╡ 64c9aa2e-9cd2-47a1-9cbd-44457ec0d095
mu = Action("move_up", 1)

# ╔═╡ 1aa09400-2e09-443d-9cbf-11b21ded0b51
md = Action("move_down", 1)

# ╔═╡ a45786a7-2e26-410d-b24f-c5a4e6687b62
co = Action("collect", 5)

# ╔═╡ fb57f66d-7d61-408c-a250-96df49343879
md"### Defining GoalsStates"

# ╔═╡ 1a5b019a-4f7b-46cf-8129-0fbc60ace48c
S1 = State("S_1", 1, [true, true, true])

# ╔═╡ f96b1a3b-a7b7-4dcb-9a20-46cd4e2a298e
S2 = State("S_2", 2, [true, true, true])

# ╔═╡ 6a4c2810-d0cc-4ead-99ea-afe9f75219ad
S3 = State("S_3", 3, [true, true, true])

# ╔═╡ 6b64b1a7-7629-4b5e-a132-d24a7de7cef6
S4 = State("S_4", 4, [true, true, true])

# ╔═╡ 8d12e442-4992-460b-b795-f7329df13894
S5 = State("S_5", 5, [true, true, true])

# ╔═╡ 3b787705-2ccc-40c6-993f-6ba008f24de1
S6 = State("S_6", 1, [false, true, true])

# ╔═╡ 4d9d3a95-409f-4223-9e0b-336159deaab9
S7 = State("S_7", 2, [false, true, true])

# ╔═╡ 3034eb74-c1a7-4303-82e9-0dd432eade90
S8 = State("S_8", 3, [false, true, true])

# ╔═╡ 72fd1663-9381-4dc6-ba1f-5b16326d7b19
S9 = State("S_9", 4, [false, true, true])

# ╔═╡ 666c33ea-7242-49a0-970b-91137459b739
S10 = State("S_10", 5, [false, true, true])

# ╔═╡ 9cd4db69-0ef5-4836-a437-3386cce064d8
S11 = State("S_11", 5, [false, false, false])

# ╔═╡ bc0e2bcc-8364-4cfe-bf02-3e91fcfa76e7
md"### Creating Transition Models"

# ╔═╡ 46d1d2f8-69fa-4c9b-81d3-b9111d43eabc
TransitionModel = Dict()

# ╔═╡ 14eee364-17d6-40d5-b5d4-bae1bcb4fc5f
push!(TransitionModel, S1 => [(mw, S2), (co, S6), (mu, S1)])

# ╔═╡ e9782b4d-83a2-4144-9016-4ff1d0cdd521
push!(TransitionModel, S1 => [(mw, S3), (co, S7), (me, S2)])

# ╔═╡ 95274eff-a4c5-4381-a24e-0b0f0ce6e25e
push!(TransitionModel, S1 => [(md, S4), (co, S8), (me, S3)])

# ╔═╡ 0a0e5b34-747c-4dbb-8aa3-1db586f3651f
push!(TransitionModel, S1 => [(mw, S5), (co, S9), (mu, S4)])

# ╔═╡ 2a790349-b747-4fcd-903d-33623494967b
push!(TransitionModel, S10 => [(mw, S5), (co, S10), (md, S11)])

# ╔═╡ 39998d9b-1c79-4fcc-8a10-7a6d04a81641
TransitionModel

# ╔═╡ dd118f9d-6be8-4dec-bc9c-38756da9f22d
md"### Inputs"

# ╔═╡ 2405b3be-63a6-4cc3-b940-3b352eca2944
begin
	md" ###
	********************************************* 
	           **** Users inputs **** 
	********************************************* 
	"
end

# ╔═╡ 67ddd889-da49-4adb-8c89-0afa8ba5eea6
begin
	md" ##### 
	********************************************* 
	    **** Enter the number of Storeys **** 
	"
end

# ╔═╡ 9cb538f0-db2b-486f-b9da-837b0e9e99b8
@bind storeys TextField()

# ╔═╡ 0ff2f4a6-d723-4e32-a42a-133b67dc213b
begin
	md"###### Number of Storeys: $storeys"
end

# ╔═╡ 16a1a3aa-9151-4e94-89d0-f99b4a05630f
begin
	md" ##### 
	********************************************* 
	    **** Enter the number of Office **** 
	"
end

# ╔═╡ 956bd867-9243-4fe6-b9c6-fa48d602122e
@bind offices TextField()

# ╔═╡ 4ae65ba9-d6b8-4347-be98-27bf7c66bd26
begin
	md"###### Number of occupied offices per floor: $offices"
end

# ╔═╡ 6ae3cac0-7d45-4fc9-bd90-8280642d1d8b
begin
	md" ##### 
	********************************************* 
	    **** Enter the number of Parcels **** 
	"
end

# ╔═╡ 1e14bf34-77e6-4269-b673-f30ff328b998
@bind parcels TextField()

# ╔═╡ 823f4076-2298-4f31-8d39-d39277ca51c1
begin
	md"###### Number of parcels in each office: $parcels"
end

# ╔═╡ 76b838fb-f909-4e8e-b66e-dc22cdbd7032
begin
	md" ##### 
	********************************************* 
	    **** Enter the Location of the agent **** 
	"
end

# ╔═╡ 70533f56-975e-4584-8338-fd7faa7ba439
@bind location TextField()

# ╔═╡ 3132f961-2ce5-4575-af7b-19547a3f2472
begin
	md"###### Starting Location of the agent is: $location"
end

# ╔═╡ 98595cce-ae8f-422d-b3ce-c549a6509b6c
md"### A star search functions"

# ╔═╡ d04ca4de-53dd-4d8a-b558-d31b6dfc8be0
function aStar(currentState, goalState)
    return distance(currentState[1] - goalState[1]) + distance(cell[2] - goalState[2])
end

# ╔═╡ bc4ed19a-a046-4cc3-99d5-47906ec2178c
function AstarSearch(TransitionModel,initialState, goalState)
	
    result = []
	frontier = Queue{State}()
	explored = []
	parents = Dict{State, State}()
	first_state = true
    enqueue!(frontier, initialState)
	parent = initialState
	
    while true
		if isempty(frontier)
			return []
		else
			current_state = dequeue!(frontier)
			push!(explored, currentState)
			candidates = TransitionModel[currentState]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(parents, single_candidate[2] => currentState)
					if (single_candiadte[2] in goal_state)
						return get_result(TransitionModel, parents,initialState, single_candidate[2])
					else
						enqueue!(frontier, single_candidate[2])
					end
				end
			end
		end
	end
end

# ╔═╡ b19ae983-ebaa-42a5-9b9a-832af0d7a2c5
function create_result(TransitionModel, ancestors, initialState, goalState)
	result = []
	explorer = goalState
	while !(explorer == initialState)
		current_state_ancestor = ancestors[explorer]
		related_transitions = TransitionModel[current_state_ancestor]
		for single_trans in related_transitions
			if single_trans[2] == explorer
				push!(result, single_trans[1])
				break
			else
				continue
			end
		end
		explorer = current_state_ancestor
	end
	return result
end

# ╔═╡ 6e386b6b-fa0e-453d-b2b9-0b093d148c33
function search(initialState, transition_dict, is_goal,all_candidates,
add_candidate, remove_candidate)
	explored = []
	ancestors = Dict{State, State}()
	the_candidates = add_candidate(all_candidates, initialState, 0)
	parent = initialState
	while true
		if isempty(the_candidates)
			return []
		else
			(t1, t2) = remove_candidate(the_candidates)
			current_state = t1
			the_candidates = t2
			push!(explored, current_state)
			candidates = transition_dict[current_state]
			for single_candidate in candidates
				if !(single_candidate[2] in explored)
					push!(ancestors, single_candidate[2] => current_state)
					if (is_goal(single_candidate[2]))
						return create_result(transition_dict, ancestors,
initialState, single_candidate[2])
					else
						the_candidates = add_candidate(the_candidates,
single_candidate[2], single_candidate[1].cost)
						end
					end
				end
			end
		end
end

# ╔═╡ a635d45f-5453-496f-9013-97b12a210e6d
function goal_test(currentState::State)
    return ! (currentState.parcel[1] || currentState.parcel[2])
end

# ╔═╡ 787f466d-79fc-4e78-988c-874a45169ae1
function add_to_queue(queue::Queue{State}, state::State, cost::Int64)
    enqueue!(queue, state)
    return queue
end

# ╔═╡ 42e18f96-4e02-44ec-a9f8-b859a68a0f80
function add_to_stack(stack::Stack{State}, state::State, cost::Int64)
    push!(stack, state)
    return stack
end

# ╔═╡ d3e2f282-1da8-4682-9f22-b2b7df96aafc
function remove_from_queue(queue::Queue{State})
    removed = dequeue!(queue)
    return (removed, queue)
end

# ╔═╡ 3bbf3504-4d80-4f2f-affd-2cb5f7a1f0d1
function remove_from_stack(stack::Stack{State})
    removed = pop!(stack)
    return (removed, stack)
end

# ╔═╡ 9b97b1a1-bfd1-4965-abc5-6c837ea3a123
search(S1, TransitionModel, goal_test, Queue{State}(), add_to_queue, remove_from_queue)

# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
DataStructures = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
InteractiveUtils = "b77e0a4c-d291-57a0-90e8-8db25a27a240"
Markdown = "d6f4376e-aef5-505a-96c1-9c027394607a"
Pkg = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"
PlutoUI = "7f904dfe-b85e-4ff6-b463-dae2292396a8"

[compat]
DataStructures = "~0.18.11"
PlutoUI = "~0.7.38"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.2"
manifest_format = "2.0"

[[deps.AbstractPlutoDingetjes]]
deps = ["Pkg"]
git-tree-sha1 = "8eaf9f1b4921132a4cff3f36a1d9ba923b14a481"
uuid = "6e696c72-6542-2067-7265-42206c756150"
version = "1.1.4"

[[deps.ArgTools]]
uuid = "0dad84c5-d112-42e6-8d28-ef12dabb789f"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.Base64]]
uuid = "2a0f44e3-6c83-55bd-87e4-b1978d98bd5f"

[[deps.ColorTypes]]
deps = ["FixedPointNumbers", "Random"]
git-tree-sha1 = "024fe24d83e4a5bf5fc80501a314ce0d1aa35597"
uuid = "3da002f7-5984-5a60-b8a6-cbb66c0b333f"
version = "0.11.0"

[[deps.Compat]]
deps = ["Base64", "Dates", "DelimitedFiles", "Distributed", "InteractiveUtils", "LibGit2", "Libdl", "LinearAlgebra", "Markdown", "Mmap", "Pkg", "Printf", "REPL", "Random", "SHA", "Serialization", "SharedArrays", "Sockets", "SparseArrays", "Statistics", "Test", "UUIDs", "Unicode"]
git-tree-sha1 = "b153278a25dd42c65abbf4e62344f9d22e59191b"
uuid = "34da2185-b29b-5c13-b0c7-acf172513d20"
version = "3.43.0"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.DataStructures]]
deps = ["Compat", "InteractiveUtils", "OrderedCollections"]
git-tree-sha1 = "3daef5523dd2e769dad2365274f760ff5f282c7d"
uuid = "864edb3b-99cc-5e75-8d2d-829cb0a9cfe8"
version = "0.18.11"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.DelimitedFiles]]
deps = ["Mmap"]
uuid = "8bb1440f-4735-579b-a4ab-409b98df4dab"

[[deps.Distributed]]
deps = ["Random", "Serialization", "Sockets"]
uuid = "8ba89e20-285c-5b6f-9357-94700520ee1b"

[[deps.Downloads]]
deps = ["ArgTools", "LibCURL", "NetworkOptions"]
uuid = "f43a241f-c20a-4ad4-852c-f6b1247861c6"

[[deps.FixedPointNumbers]]
deps = ["Statistics"]
git-tree-sha1 = "335bfdceacc84c5cdf16aadc768aa5ddfc5383cc"
uuid = "53c48c17-4a7d-5ca2-90c5-79b7896eea93"
version = "0.8.4"

[[deps.Hyperscript]]
deps = ["Test"]
git-tree-sha1 = "8d511d5b81240fc8e6802386302675bdf47737b9"
uuid = "47d2ed2b-36de-50cf-bf87-49c2cf4b8b91"
version = "0.0.4"

[[deps.HypertextLiteral]]
git-tree-sha1 = "2b078b5a615c6c0396c77810d92ee8c6f470d238"
uuid = "ac1192a8-f4b3-4bfe-ba22-af5b92cd3ab2"
version = "0.9.3"

[[deps.IOCapture]]
deps = ["Logging", "Random"]
git-tree-sha1 = "f7be53659ab06ddc986428d3a9dcc95f6fa6705a"
uuid = "b5f81e59-6552-4d32-b1f0-c071b021bf89"
version = "0.2.2"

[[deps.InteractiveUtils]]
deps = ["Markdown"]
uuid = "b77e0a4c-d291-57a0-90e8-8db25a27a240"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LibCURL]]
deps = ["LibCURL_jll", "MozillaCACerts_jll"]
uuid = "b27032c2-a3e7-50c8-80cd-2d36dbcbfd21"

[[deps.LibCURL_jll]]
deps = ["Artifacts", "LibSSH2_jll", "Libdl", "MbedTLS_jll", "Zlib_jll", "nghttp2_jll"]
uuid = "deac9b47-8bc7-5906-a0fe-35ac56dc84c0"

[[deps.LibGit2]]
deps = ["Base64", "NetworkOptions", "Printf", "SHA"]
uuid = "76f85450-5226-5b5a-8eaa-529ad045b433"

[[deps.LibSSH2_jll]]
deps = ["Artifacts", "Libdl", "MbedTLS_jll"]
uuid = "29816b5a-b9ab-546f-933c-edad1886dfa8"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Logging]]
uuid = "56ddb016-857b-54e1-b83d-db4d58db5568"

[[deps.Markdown]]
deps = ["Base64"]
uuid = "d6f4376e-aef5-505a-96c1-9c027394607a"

[[deps.MbedTLS_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "c8ffd9c3-330d-5841-b78e-0817d7145fa1"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.MozillaCACerts_jll]]
uuid = "14a3606d-f60d-562e-9121-12d972cd8159"

[[deps.NetworkOptions]]
uuid = "ca575930-c2e3-43a9-ace4-1e988b2c1908"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.OrderedCollections]]
git-tree-sha1 = "85f8e6578bf1f9ee0d11e7bb1b1456435479d47c"
uuid = "bac558e1-5e72-5ebc-8fee-abe8a469f55d"
version = "1.4.1"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "1285416549ccfcdf0c50d4997a94331e88d68413"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.3.1"

[[deps.Pkg]]
deps = ["Artifacts", "Dates", "Downloads", "LibGit2", "Libdl", "Logging", "Markdown", "Printf", "REPL", "Random", "SHA", "Serialization", "TOML", "Tar", "UUIDs", "p7zip_jll"]
uuid = "44cfe95a-1eb2-52ea-b672-e2afdf69b78f"

[[deps.PlutoUI]]
deps = ["AbstractPlutoDingetjes", "Base64", "ColorTypes", "Dates", "Hyperscript", "HypertextLiteral", "IOCapture", "InteractiveUtils", "JSON", "Logging", "Markdown", "Random", "Reexport", "UUIDs"]
git-tree-sha1 = "670e559e5c8e191ded66fa9ea89c97f10376bb4c"
uuid = "7f904dfe-b85e-4ff6-b463-dae2292396a8"
version = "0.7.38"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.REPL]]
deps = ["InteractiveUtils", "Markdown", "Sockets", "Unicode"]
uuid = "3fa0cd96-eef1-5676-8a61-b3b8758bbffb"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.Reexport]]
git-tree-sha1 = "45e428421666073eab6f2da5c9d310d99bb12f9b"
uuid = "189a3867-3050-52da-a836-e630ba90ab69"
version = "1.2.2"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.SharedArrays]]
deps = ["Distributed", "Mmap", "Random", "Serialization"]
uuid = "1a1011a3-84de-559e-8e89-a11a2f7dc383"

[[deps.Sockets]]
uuid = "6462fe0b-24de-5631-8697-dd941f90decc"

[[deps.SparseArrays]]
deps = ["LinearAlgebra", "Random"]
uuid = "2f01184e-e22b-5df5-ae63-d93ebab69eaf"

[[deps.Statistics]]
deps = ["LinearAlgebra", "SparseArrays"]
uuid = "10745b16-79ce-11e8-11f9-7d13ad32a3b2"

[[deps.TOML]]
deps = ["Dates"]
uuid = "fa267f1f-6049-4f14-aa54-33bafae1ed76"

[[deps.Tar]]
deps = ["ArgTools", "SHA"]
uuid = "a4e569a6-e804-4fa4-b0f3-eef7a1d5b13e"

[[deps.Test]]
deps = ["InteractiveUtils", "Logging", "Random", "Serialization"]
uuid = "8dfed614-e22c-5e08-85e1-65c5234f0b40"

[[deps.UUIDs]]
deps = ["Random", "SHA"]
uuid = "cf7118a7-6976-5b1a-9a39-7adc72f591a4"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Zlib_jll]]
deps = ["Libdl"]
uuid = "83775a58-1f1d-513f-b197-d71354ab007a"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"

[[deps.nghttp2_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "8e850ede-7688-5339-a07c-302acd2aaf8d"

[[deps.p7zip_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "3f19e933-33d8-53b3-aaab-bd5110c3b7a0"
"""

# ╔═╡ Cell order:
# ╠═1d776b4a-1d8b-4607-b984-34b2005fd007
# ╠═2e727944-794f-46f7-b80a-8774333f8f2a
# ╠═dc8c4de1-0f87-42d4-94c8-b2730dca78d9
# ╠═c40ffb8b-f1f6-48b2-892b-3104f8b14e48
# ╠═6f0c6169-02fa-4ef3-89f7-010b4e589829
# ╠═b7802b24-5088-4b72-b5fd-136af885cbd0
# ╠═493da480-1b9b-451b-909a-eeaa53fc51ed
# ╠═9d97ebfa-4d7a-441f-92c1-e5724855f5b8
# ╠═1b1461de-b58f-45a8-9043-bf6e0b10749b
# ╠═2d6488f7-743b-44af-87be-d535f5b0924a
# ╠═8dc02067-c4c1-4b5f-879e-e181530e8b4d
# ╠═c35c205e-36c8-4faf-8578-7711cd9ba8b8
# ╠═64c9aa2e-9cd2-47a1-9cbd-44457ec0d095
# ╠═1aa09400-2e09-443d-9cbf-11b21ded0b51
# ╠═a45786a7-2e26-410d-b24f-c5a4e6687b62
# ╠═fb57f66d-7d61-408c-a250-96df49343879
# ╠═1a5b019a-4f7b-46cf-8129-0fbc60ace48c
# ╠═f96b1a3b-a7b7-4dcb-9a20-46cd4e2a298e
# ╠═6a4c2810-d0cc-4ead-99ea-afe9f75219ad
# ╠═6b64b1a7-7629-4b5e-a132-d24a7de7cef6
# ╠═8d12e442-4992-460b-b795-f7329df13894
# ╠═3b787705-2ccc-40c6-993f-6ba008f24de1
# ╠═4d9d3a95-409f-4223-9e0b-336159deaab9
# ╠═3034eb74-c1a7-4303-82e9-0dd432eade90
# ╠═72fd1663-9381-4dc6-ba1f-5b16326d7b19
# ╠═666c33ea-7242-49a0-970b-91137459b739
# ╠═9cd4db69-0ef5-4836-a437-3386cce064d8
# ╠═bc0e2bcc-8364-4cfe-bf02-3e91fcfa76e7
# ╠═46d1d2f8-69fa-4c9b-81d3-b9111d43eabc
# ╠═14eee364-17d6-40d5-b5d4-bae1bcb4fc5f
# ╠═e9782b4d-83a2-4144-9016-4ff1d0cdd521
# ╠═95274eff-a4c5-4381-a24e-0b0f0ce6e25e
# ╠═0a0e5b34-747c-4dbb-8aa3-1db586f3651f
# ╠═2a790349-b747-4fcd-903d-33623494967b
# ╠═39998d9b-1c79-4fcc-8a10-7a6d04a81641
# ╠═dd118f9d-6be8-4dec-bc9c-38756da9f22d
# ╟─2405b3be-63a6-4cc3-b940-3b352eca2944
# ╟─67ddd889-da49-4adb-8c89-0afa8ba5eea6
# ╠═9cb538f0-db2b-486f-b9da-837b0e9e99b8
# ╟─0ff2f4a6-d723-4e32-a42a-133b67dc213b
# ╟─16a1a3aa-9151-4e94-89d0-f99b4a05630f
# ╠═956bd867-9243-4fe6-b9c6-fa48d602122e
# ╟─4ae65ba9-d6b8-4347-be98-27bf7c66bd26
# ╟─6ae3cac0-7d45-4fc9-bd90-8280642d1d8b
# ╠═1e14bf34-77e6-4269-b673-f30ff328b998
# ╟─823f4076-2298-4f31-8d39-d39277ca51c1
# ╟─76b838fb-f909-4e8e-b66e-dc22cdbd7032
# ╠═70533f56-975e-4584-8338-fd7faa7ba439
# ╟─3132f961-2ce5-4575-af7b-19547a3f2472
# ╠═98595cce-ae8f-422d-b3ce-c549a6509b6c
# ╠═d04ca4de-53dd-4d8a-b558-d31b6dfc8be0
# ╠═bc4ed19a-a046-4cc3-99d5-47906ec2178c
# ╠═b19ae983-ebaa-42a5-9b9a-832af0d7a2c5
# ╠═6e386b6b-fa0e-453d-b2b9-0b093d148c33
# ╠═a635d45f-5453-496f-9013-97b12a210e6d
# ╠═787f466d-79fc-4e78-988c-874a45169ae1
# ╠═42e18f96-4e02-44ec-a9f8-b859a68a0f80
# ╠═d3e2f282-1da8-4682-9f22-b2b7df96aafc
# ╠═3bbf3504-4d80-4f2f-affd-2cb5f7a1f0d1
# ╠═9b97b1a1-bfd1-4965-abc5-6c837ea3a123
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
